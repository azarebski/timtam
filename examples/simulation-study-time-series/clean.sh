#!/usr/bin/env bash

rm out/*csv
rm out/*png
rm out/*pdf
rm out/*txt

rm ts-config.json
