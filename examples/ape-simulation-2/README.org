#+title: Simulating aggregated data with ape

To make the =index.html= file to view the output use the =index= recipe

#+begin_src sh
make index
#+end_src

To clean up the results of previous computation use the =clean= recipe

#+begin_src sh
make clean
#+end_src

To view the results open =index.html= with your browser.

This runs a single simulation and then analyses the data once using the exact
sampling and occurrence times and then a second time using the aggregated
values. Some diagnostics are also produced to help inspect the results.

* Results

#+attr_org: :width 500
[[./out/ape-sim-figures-combined.png]]

#+attr_org: :width 500
[[./out/marginal-distributions-combined.png]]
